import os
from pdfworks_lib.pdfworks import Converter
from datetime import datetime


# precheck INPUT and OUTPUT dirs
for directory in ['INPUT', 'OUTPUT']:
	if not os.path.exists(f"./{directory}"):
		os.mkdir(f"./{directory}")


# detect files in INPUT
converter = Converter()
supported_img_formats = converter.SUPPORTED_IMAGE_FILE_FORMATS + ['.pdf']
input_files = [file for file in os.listdir("./INPUT") if file.lower().endswith(tuple(supported_img_formats))]
input_files = [os.path.join("INPUT", file) for file in input_files]


# merge 'em to file in OUTPUT
output_file = os.path.join("OUTPUT", f"{datetime.now():%Y-%m-%d_%H-%M-%S--%f}.pdf")
print(f"merging:\r\n{input_files}\r\ninto: {output_file}")
try:
	converter.convert(input_files, output_file)
	print("everything seems to be okay")
except Exception as e:
	print("something went WRONG")
	print(f"error message: {e}")
