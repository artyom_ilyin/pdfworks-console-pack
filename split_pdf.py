import os
from pdfworks_lib.pdfworks import Converter


# precheck INPUT and OUTPUT dirs
for directory in ['INPUT', 'OUTPUT']:
	if not os.path.exists(f"./{directory}"):
		os.mkdir(f"./{directory}")


# detect files in INPUT
converter = Converter()
input_files = [file for file in os.listdir("./INPUT") if file.lower().endswith('.pdf')]
input_files = [os.path.join("INPUT", file) for file in input_files]


# split 'em one by one to OUTPUT
for file in input_files:
	try:
		print(f"splitting: {file}")
		converter.split_pdf(file, "OUTPUT")
		print("everything seems to be okay")
	except Exception as e:
		print(f"something went WRONG with splitting {file}")
		print(f"error message: {e}")
